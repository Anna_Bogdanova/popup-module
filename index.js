var myApp = angular.module('myApp', []);

var html = `<div id="moduleWindow">
		   <div id="moduleHeader">
		     <span id="moduleTitle" ng-click="open=true">{{title}}</span>
		     <span id="close" ng-show="open" ng-click="open=fasle">x</span>
		   </div>
         <hr>
         <form ng-show="open">
            <div id="moludeContent" ng-transclude></div>
            <hr>
            <button type="submit" class="editBut submitBut" ng-click="open=false">Submit</button>
            <button type="button" class="editBut cancelBut" ng-click="open=false">Cancel</button>
         </form>
   </div>`;


angular.module('myApp').directive('myDir', function() {
     return {
     restrict: 'E',
     template: html,
     transclude: true,
     scope: {
        title: '@',
        onSubbmit: '&',
        open: '&'
    },
 }
}

);